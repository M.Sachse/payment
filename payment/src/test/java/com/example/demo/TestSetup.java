package com.example.demo;

import com.paypal.http.HttpClient;
import com.paypal.core.PayPalHttpClient;
import com.paypal.core.PayPalEnvironment;
import org.junit.jupiter.api.BeforeEach;

public class TestSetup {

    private HttpClient client;
    private PayPalEnvironment environment;
    private final String CLIENT_ID = "AUkPAC8h8ohpOzVCFSlJfPL1b58p5NGsyRtiOEv3InGBUAT6rMXe_5_LiUGzc9Uqsfpu46fe1AB8QeSW";
    private final String CLIENT_SECRET = "EDwjO2Exj1E1FCGn1zER9LrKY65uP0TN-wdLiAOd97C85142gzRVYvYtVXHdjZd0PZVHIKEfnOu-BXlA";

    @BeforeEach
    public void setup() {
        environment = new PayPalEnvironment.Sandbox(CLIENT_ID, CLIENT_SECRET);
        client = new PayPalHttpClient(environment);
    }

    protected HttpClient client() {
        return client;
    }

    protected PayPalEnvironment environment() {
        return environment;
    }
}
