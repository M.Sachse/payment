package com.example.demo.services;

import com.example.demo.TestSetup;
import com.paypal.http.HttpClient;
import com.paypal.http.HttpResponse;
import com.paypal.orders.*;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreateOrderTest extends TestSetup {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public static OrderRequest buildMinimumRequestBody() {
        OrderRequest request = new OrderRequest();
        request.checkoutPaymentIntent("AUTHORIZE");

        List<PurchaseUnitRequest> units= new ArrayList<>();
        PurchaseUnitRequest unit = new PurchaseUnitRequest()
                .amountWithBreakdown(new AmountWithBreakdown()
                        .currencyCode("EUR")
                        .value("100.00"));
        units.add(unit);
        request.purchaseUnits(units);

        return request;
    }

    public static Order createOrder(HttpClient client) throws IOException {
        OrdersCreateRequest request = new OrdersCreateRequest();
        request.requestBody(buildMinimumRequestBody());
        request.prefer("return=representation");
        HttpResponse<Order> response = client.execute(request);

        assertEquals("CREATED", response.result().status());
        assertEquals(response.statusCode(), 201);

        Order createdOrder = response.result();
        assertNotNull(createdOrder);

        return createdOrder;
    }

    @Test
    public void createOrder() throws IOException {

        //When
        Order createdOrder = createOrder(client());

        //Then
        for (LinkDescription link : createdOrder.links()) {
            LOGGER.info("\t" + link.rel() + ": " + link.href());
        }
        assertEquals("CREATED", createdOrder.status());
        assertNotNull(createdOrder.id());
        assertNotNull(createdOrder.purchaseUnits());
        assertNotNull(createdOrder.links());
    }
}
