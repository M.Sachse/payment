package com.example.demo.services;

import com.example.demo.TestSetup;
import com.paypal.http.HttpResponse;
import com.paypal.orders.Order;
import com.paypal.orders.OrdersCaptureRequest;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CaptureOrderTest extends TestSetup {

    @Test
    @Disabled("Skipped since there is no valid payment_source or approved order id")
    public void captureOrder() throws IOException {

        //Given
        Order order = CreateOrderTest.createOrder(client());
        /*
         * the Token is not working. The ID and the Type should be generated from Paypal
         * the docu of PayPal does not clearly discripe what to do for this informations
         * Therefore, this test does not work -> you can also use a order_id which is in status 'approved'

         Card card = new Card();
         card.number("4020022728836752");
         card.expiry("02/2027");

         Token token = new Token();
         token.id("CARD-4020022728836752");
         token.type("PAYMENT_METHOD_TOKEN");
         */

        //When
        OrdersCaptureRequest request = new OrdersCaptureRequest(order.id());
        /*
         * depends on token problem
        PaymentSource source = new PaymentSource();
        source.card(card);
        source.token(token);
        request.requestBody(source);
         */
        HttpResponse<Order> response = client().execute(request);

        //Then
        assertEquals(response.statusCode(), 201);
        assertNotNull(response.result());
        assertEquals("COMPLETED", response.result().status());
    }
}
