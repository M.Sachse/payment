package com.example.demo.services;

import com.example.demo.TestSetup;
import com.paypal.http.HttpResponse;
import com.paypal.orders.Order;
import com.paypal.orders.OrdersGetRequest;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ShowOrderDetailsTest extends TestSetup {

    @Test
    public void checkCreatedOrder() throws IOException {

        //Given
        Order order = CreateOrderTest.createOrder(client());

        //When
        OrdersGetRequest createdOrderRequest = new OrdersGetRequest(order.id());
        HttpResponse<Order> response = client().execute(createdOrderRequest);

        order = response.result();

        //Then
        assertThat(response.statusCode() == 200);
        assertNotNull(response.result());

        assertEquals("CREATED", order.status());
        assertNotNull(order.id());
        assertNotNull(order.purchaseUnits());
        assertNotNull(order.links());
    }
}
