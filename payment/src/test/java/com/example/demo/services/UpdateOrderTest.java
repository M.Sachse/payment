package com.example.demo.services;

import com.example.demo.TestSetup;
import com.paypal.http.HttpResponse;
import com.paypal.orders.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UpdateOrderTest extends TestSetup {

    public static List<Patch> buildPatchRequestBody() throws IOException {
        List<Patch> patches = new ArrayList<>();
        patches.add(new Patch()
                .op("add")
                .path("/purchase_units/@reference_id=='default'/description")
                .value("added_description"));
        patches.add(new Patch()
                .op("replace")
                .path("/purchase_units/@reference_id=='default'/amount")
                .value(new AmountWithBreakdown()
                        .currencyCode("EUR")
                        .value("50.00")));
        return patches;
    }

    @Test
    public void updateOrder() throws IOException {

        //Given
        Order order = CreateOrderTest.createOrder(client());

        //When
        OrdersPatchRequest request = new OrdersPatchRequest(order.id());
        request.requestBody(buildPatchRequestBody());
        HttpResponse<Void> response = client().execute(request);

        OrdersGetRequest getRequest = new OrdersGetRequest(order.id());
        HttpResponse<Order> getResponse = client().execute(getRequest);
        order = getResponse.result();

        //Then
        assertEquals(response.statusCode(), 204);

        assertEquals(getResponse.statusCode(), 200);

        assertNotNull(order.id());
        assertNotNull(order.purchaseUnits());
        assertNotNull(order.links());
        PurchaseUnit firstPurchaseUnit = order.purchaseUnits().get(0);
        assertEquals(firstPurchaseUnit.amountWithBreakdown().value(), "50.00");
        assertEquals(firstPurchaseUnit.description(), "added_description");

    }
}
