package com.example.demo.entities;

import java.util.Objects;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User {

	int userId;
	String firstName;
	String lastName;
	String eMail;
	String password;
	String street;
	String houseNo;
	String city;
	String state;
	String zipCode;
	String contry;
	
	public User() {

	}

	public User(int userId, String firstName, String lastName, String eMail, String password,
			String street, String houseNo, String city, String state, String zipCode, String contry) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eMail = eMail;
		this.password = password;
		this.street = street;
		this.houseNo = houseNo;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.contry = contry;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getFirstName() {
		return firstName;
	}
		
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String geteMail() {
		return eMail;
	}
	
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getContry() {
		return contry;
	}

	public void setContry(String contry) {
		this.contry = contry;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", eMail=" + eMail
				+ ", password=" + password + ", street=" + street + ", houseNo=" + houseNo + ", city=" + city
				+ ", state=" + state + ", zipCode=" + zipCode + ", contry=" + contry + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(city, contry, eMail, firstName, houseNo, lastName, password, state, street, zipCode);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(city, other.city) && Objects.equals(contry, other.contry)
				&& Objects.equals(eMail, other.eMail) && Objects.equals(firstName, other.firstName)
				&& Objects.equals(houseNo, other.houseNo) && Objects.equals(lastName, other.lastName)
				&& Objects.equals(password, other.password) && Objects.equals(state, other.state)
				&& Objects.equals(street, other.street) && Objects.equals(zipCode, other.zipCode);
	}
	
}
