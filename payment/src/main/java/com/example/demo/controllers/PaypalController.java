package com.example.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v2/checkout/orders")
public class PaypalController {

	@GetMapping
	public String getMassega() {
		return "Hallo";
	}
	
	@GetMapping("/withText")
	public String getMassege(String text) {
		return text;
	}
}
