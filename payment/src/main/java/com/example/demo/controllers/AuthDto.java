package com.example.demo.controllers;

import java.util.Objects;

public class AuthDto {

	private String eMail;
	private String password;
	
	public AuthDto(String eMail, String password) {
		this.eMail = eMail;
		this.password = password;
	}
	
	public AuthDto() {

	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		return Objects.hash(eMail, password);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthDto other = (AuthDto) obj;
		return Objects.equals(eMail, other.eMail) && Objects.equals(password, other.password);
	}
	
	
}
