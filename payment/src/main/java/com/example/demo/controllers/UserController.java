package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.User;
import com.example.demo.services.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping
	public User getUser(String email, String password) {
		return userService.getUser(email, password);
	}

	@PutMapping
	public User updateUser(@RequestBody AuthDto credentials) {
		return userService.update(credentials);
	}

	@PostMapping
	public String createUser(@RequestBody User user) {
		return userService.createUser(user);
	}

	@PostMapping("/signin")
	public String login(@RequestBody AuthDto credentials) {
		return userService.login(credentials);
	}

	@DeleteMapping
	public String deleteUser(@RequestBody AuthDto credentials) {
		return userService.deleteUser(credentials);
	}
}
