package com.example.demo.repos;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.entities.User;

public interface UserRepo extends CrudRepository<User, Integer>{

	User findByeMail(String eMail);
	Boolean existsByeMail(String eMail);

}
