package com.example.demo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.controllers.AuthDto;
import com.example.demo.entities.User;
import com.example.demo.repos.UserRepo;

@Service
public class UserService {

	@Autowired
	private UserRepo userRepo;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	public String createUser(User user) {
		
		if (userRepo.existsByeMail(user.geteMail())) {
			return "EMail is already present";	
		}
		
		userRepo.save(user);
		return "User is saved";
	}
	
	public Boolean authenticate(AuthDto credentials) {
		if (!userRepo.existsByeMail(credentials.geteMail())) {
			LOGGER.info("EMail is not known!");
			return false;	
		}
		else if (!checkIfPasswordMatchEMail(credentials.geteMail(), credentials.getPassword())) {
			LOGGER.info("Password is wrong!");
			return false;
		}
		LOGGER.info("Your credentials match!");
		return true;
	}
	
	public String login(AuthDto credentials) {
		
		if (authenticate(credentials)) {
			return "You are logged in";
		};
		return "Something went wrong while log in!";
	}
	
	public String deleteUser(AuthDto credentials) {
		
		if (authenticate(credentials)) {
			User user = userRepo.findByeMail(credentials.geteMail());
			userRepo.delete(user);
			return "User is deleted";
		}
		return "Something went wrong while delete user!";	
	}

	public User update(AuthDto credentials) {
		return null;
	}

	public User getUser(String email, String password) {
		return null;
	}
	
	public Boolean checkIfPasswordMatchEMail(String email, String password) {
		User user = userRepo.findByeMail(email);
		if (!user.getPassword().equals(password)) {
			return false;
		}
		return true;
	}

}
